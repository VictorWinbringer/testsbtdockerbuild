FROM hseeberger/scala-sbt:11.0.2-oraclelinux7_1.3.12_2.13.3 AS base
COPY . /root
WORKDIR /root
RUN sbt test
RUN sbt stage

FROM openjdk:16 as final
COPY --from=base /root/target/universal/stage /root
WORKDIR /root/bin
EXPOSE 8080
ENTRYPOINT ["sh","movables"]