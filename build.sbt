name := "Movables"

version := "0.2"

scalaVersion := "2.13.3"

enablePlugins(JavaAppPackaging)

libraryDependencies ++= Seq(
  "co.fs2" %% "fs2-core" % "2.4.2",
  "co.fs2" %% "fs2-io" % "2.4.2",
  "org.scalatest" %% "scalatest" % "3.0.8" % Test
)