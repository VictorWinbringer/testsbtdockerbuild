

trait Movable[A] {
  def move(a: A)(): A
}

object Movable {

  implicit class MovableOps[A](a: A)(implicit val movable: Movable[A]) {
    def move(): A = movable.move(a)()
  }

}

trait Collaidable[A, B] {
  def collide(a: A)(b: B): Boolean
}

object Collaidable {

  implicit class CollaidableOps[A, B](a: A)(implicit val collaidable: Collaidable[A, B]) {
    def collide(b: B): Boolean = collaidable.collide(a)(b)
  }

}

case class Tank()

case class Car()

class TankService extends Movable[Tank] with Collaidable[Tank, Car] {
  def move(a: Tank)(): Tank = a

  def collide(a: Tank)(b: Car): Boolean = true
}

class CarService extends Movable[Car] with Collaidable[Car, Tank] {
  def move(a: Car)(): Car = a

  def collide(a: Car)(b: Tank): Boolean = true
}

class GameService[A, B](a: A, b: B)(implicit val ma: Movable[A], implicit val mb: Movable[B], implicit val cb: Collaidable[A, B]) {

  import Movable._
  import Collaidable._

  def run() = a.move().collide(b.move())
}

import cats.Functor
import cats.effect.{Concurrent, ContextShift, ExitCode, IO, IOApp}
import fs2.Stream
import fs2.concurrent.Queue
import cats.implicits._
import cats._

import scala.concurrent.ExecutionContext


object Game extends App {
  def splitStream[F[_], A](stream: Stream[F, A])(implicit c: Concurrent[F], f: Functor[F]): F[(Stream[F, A], Stream[F, A])] = {
    for {
      q <- Queue.noneTerminated[F, A]
    } yield (stream.evalTap(a => q.enqueue1(Some(a))).onFinalize(q.enqueue1(None)), q.dequeue)
  }

  implicit val st = new TankService
  implicit val sc = new CarService
  val tank = Tank()
  val car = Car()
  val gs = new GameService[Tank, Car](tank, car)
  println(gs.run())

  import fs2.Stream

  implicit val contextShift: ContextShift[IO] = IO.contextShift(ExecutionContext.global)
  var sr = Stream(1, 2, 3, 4)
    .map(x => (Stream.emit(x), Stream.emit(x)))
    .map(a => (a._1.compile.toVector, a._2.compile.toVector))
    .compile
    .toVector
  println(sr)
  var sr2 = for {
    ss <- splitStream(Stream(1, 2, 3, 4).covary[IO])
    s1 <- ss._1.compile.toVector
    s2 <- ss._2.compile.toVector
  } yield (s1, s2)
  println(sr2.unsafeRunSync())
}